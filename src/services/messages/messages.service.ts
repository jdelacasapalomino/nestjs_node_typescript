import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateMessageDTO } from 'src/dto/create-message.dto';
import { Message } from 'src/entity/message.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MessagesService {
  constructor(
    @InjectRepository(Message)
    private readonly messageRepository: Repository<Message>,
  ) {}

  async getAll() {
    return await this.messageRepository.find();
  }

  async createMessage(newMessage: CreateMessageDTO) {
    const nuevo = new Message();
    // para evitar que se inserten datos no deseados/id
    nuevo.message = newMessage.message;
    nuevo.nick = newMessage.nick;

    return await this.messageRepository.save(nuevo);
  }

  async updateMessage(idMessage: number, updatedMessage: CreateMessageDTO) {
    const message = await this.messageRepository.findOne(idMessage);
    message.message = updatedMessage.message;
    message.nick = updatedMessage.nick;

    return await this.messageRepository.save(message);
  }

  async deleteMessage(idMessage: number) {
    return this.messageRepository.delete(idMessage);
  }
}
