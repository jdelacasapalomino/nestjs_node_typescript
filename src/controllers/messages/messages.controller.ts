import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
} from '@nestjs/common';
import { response } from 'express';
import { MessagesService } from 'src/services/messages/messages.service';
import { CreateMessageDTO } from '../../dto/create-message.dto';

@Controller('messages')
export class MessagesController {
  constructor(private readonly messageService: MessagesService) {}

  @Post()
  create(@Body() createMessageDto: CreateMessageDTO, @Res() res) {
    return this.messageService
      .createMessage(createMessageDto)
      .then((message) => {
        res.status(HttpStatus.CREATED).json(message);
      })
      .catch(() => {
        response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json({ error: 'Unknown Error' });
      });
  }

  @Get()
  getAll(@Res() res) {
    this.messageService
      .getAll()
      .then((messages) => {
        res.status(HttpStatus.OK).json(messages);
      })
      .catch(() => {
        response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json({ error: 'Unknown Error' });
      });
  }

  @Put(':id')
  update(
    @Body() updateMessageDto: CreateMessageDTO,
    @Res() res,
    @Param('id') idMessage,
  ) {
    this.messageService
      .updateMessage(idMessage, updateMessageDto)
      .then((message) => {
        res.status(HttpStatus.OK).json(message);
      })
      .catch(() => {
        response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json({ error: 'Unknown Error' });
      });
  }

  @Delete(':id')
  delete(@Res() res, @Param('id') idMessage) {
    this.messageService
      .deleteMessage(idMessage)
      .then((message) => {
        res.status(HttpStatus.OK).json(message);
      })
      .catch(() => {
        response
          .status(HttpStatus.INTERNAL_SERVER_ERROR)
          .json({ error: 'Unknown Error' });
      });
  }
}
