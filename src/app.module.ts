import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MessagesController } from './controllers/messages/messages.controller';
import { Message } from './entity/message.entity';
import { MessagesService } from './services/messages/messages.service';

@Module({
  imports: [TypeOrmModule.forRoot(), TypeOrmModule.forFeature([Message])],
  controllers: [AppController, MessagesController],
  providers: [AppService, MessagesService],
})
export class AppModule {}
