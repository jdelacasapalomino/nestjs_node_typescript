-- Adminer 4.8.0 MySQL 5.6.51 dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP DATABASE IF EXISTS `messageapp`;
CREATE DATABASE `messageapp` /*!40100 DEFAULT CHARACTER SET utf16 COLLATE utf16_spanish2_ci */;
USE `messageapp`;

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(255) COLLATE utf16_spanish2_ci NOT NULL,
  `message` varchar(255) COLLATE utf16_spanish2_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf16 COLLATE=utf16_spanish2_ci;

INSERT INTO `messages` (`id`, `nick`, `message`) VALUES
(1,	'Test',	'Prueba msg'),
(2,	'Prueba',	'Prueba msg 2'),
(3,	'Prueba',	'Prueba msg 3');

-- 2021-03-23 18:45:25
