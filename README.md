# MessageApp

[Udemy course](https://www.udemy.com/course/nestjs-free/)

## Running the app

```bash

# Download modules
$ npm install

# Start DB
$ docker-compose up

# development
$ npm run start

```

## Postman Requests

- [docs/MessageApp.postman_collection.json](./docs/MessageApp.postman_collection.json)


